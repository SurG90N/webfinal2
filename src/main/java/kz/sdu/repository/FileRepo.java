package kz.sdu.repository;

import kz.sdu.model.FinalFile;
import org.springframework.data.repository.CrudRepository;
//asdas

public interface FileRepo extends CrudRepository<FinalFile, Integer> {
    Iterable<FinalFile> findByFiledate(String date);
    Iterable<FinalFile> findByPath(String path);
}
