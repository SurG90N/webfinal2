package kz.sdu.controller;

import kz.sdu.model.FinalFile;
import kz.sdu.repository.FileRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@Controller
@RequestMapping(path="/project")
public class MyController {

    @Autowired
    private FileRepo fileRepo;

    public static String UPLOAD_DIR = "/home/saken/IdeaProjects/project-adverts/upload/";

    @RequestMapping(value = "/file", method = RequestMethod.PUT)
    public ResponseEntity fileUpload(@RequestParam("file") MultipartFile file) {
        try {
            int last = file.getOriginalFilename().lastIndexOf(".");
            String ext = file.getOriginalFilename().substring(last+1).toLowerCase();
            System.out.println(ext);
            byte[] bytes = file.getBytes();

            if(ext.equals("txt")){
                Date date = new Date();
                FinalFile uploadfile= new FinalFile();
                String spam = new String(file.getBytes());
                if (spam.contains("spam")){
                    File direct = new File(UPLOAD_DIR+"spam");
                    if (!direct.exists()) {
                        if (direct.mkdir()) {
                            System.out.println("Directory is created!");
                        } else {
                            System.out.println("Failed to create directory!");
                        }
                    }
                    String mypath = direct+"/"+date.getTime()+"."+ext;
                    uploadfile.setFilepath(mypath);
                    uploadfile.setFilename(file.getOriginalFilename());
                    uploadfile.setPath("spam");
                    fileRepo.save(uploadfile);
                    Path path = Paths.get(mypath);
                    Files.write(path, bytes);
                }
                else {
                    Date dates = new Date(); // your date
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(dates);
                    String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
                    int year = cal.get(Calendar.YEAR);
                    int month = cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    String nowDay = String.valueOf(day + "/" + monthNames[month] + "/" + year);

                    System.out.println(nowDay);
                    File direct = new File(UPLOAD_DIR + nowDay);
                    if (!direct.exists()) {
                        if (direct.mkdir()) {
                            System.out.println("Directory is created!");
                        } else {
                            System.out.println("Failed to create directory!");
                        }
                    }
                    String mypath = direct+"/"+date.getTime()+"."+ext;
                    uploadfile.setFilepath(mypath);
                    uploadfile.setFilename(file.getOriginalFilename());
                    uploadfile.setFiledate(nowDay);
                    uploadfile.setPath(nowDay);
                    fileRepo.save(uploadfile);
                    Path path = Paths.get(mypath);
                    Files.write(path, bytes);
                    System.out.println("Added");
                }
            }

        } catch (IOException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @GetMapping(path="/alldates")
    public @ResponseBody ArrayList<String> getDates(@RequestParam String date) {
        ArrayList<String> asd = new ArrayList<>();
        for(FinalFile f : fileRepo.findByFiledate(date)){
            asd.add(f.getFilename());
        }
        return asd;
    }
    @GetMapping(path="/allspams")
    public @ResponseBody ArrayList<String> getSpams(@RequestParam String path) {
        ArrayList<String> asd = new ArrayList<>();
        for(FinalFile f : fileRepo.findByPath(path)){
            asd.add(f.getFilename());
        }
        return asd;
    }

    @DeleteMapping(path="/deletespam")
    public @ResponseBody ResponseEntity deleteSpam(@RequestParam String path) {
        ArrayList<Integer> asd = new ArrayList<>();
        for(FinalFile f : fileRepo.findByPath(path)){
            fileRepo.delete(f.getId());
        }
        return new ResponseEntity(HttpStatus.ACCEPTED);

    }
}
//as