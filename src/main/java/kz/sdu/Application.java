package kz.sdu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by asxa on 3/19/17.
 */

@SpringBootApplication
//@ComponentScan({"kz.sdu.repository.CategoryRepo"})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}//as